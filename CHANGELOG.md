# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.1.0]
### Added
- This changelog file.
- The readme file.
- The initial system's archtecture. 


[unreleased]: https://bitbucket.org/keycar/pool-corporate/branches/compare/HEAD..v0.1.0
[v0.1.0]: https://bitbucket.org/keycar/pool-corporate/commits/v0.1.0

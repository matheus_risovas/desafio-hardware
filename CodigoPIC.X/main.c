#pragma config FOSC = INTOSCIO  // Oscillator Selection bits (INTOSC oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF     // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = OFF      // RA5/MCLR/VPP Pin Function Select bit (RA5/MCLR/VPP pin function is digital input, MCLR internally tied to VDD)
#pragma config BOREN = OFF      // Brown-out Detect Enable bit (BOD disabled)
#pragma config LVP = OFF         // Low-Voltage Programming Enable bit (RB4/PGM pin has PGM function, low-voltage programming enabled)
#pragma config CPD = OFF        // Data EE Memory Code Protection bit (Data memory code protection off)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

#include <xc.h>
#include <pic16f628a.h>
#include <stdbool.h>

#define _XTAL_FREQ 4000000

#define TEMPO_DELAY 500
#define TEMPO_DELAY2 50 
#define TEMPO_DELAY3 100
#define TEMPO_DELAY4 150

void delay(int tempoDelay){
    while(tempoDelay--){
        __delay_ms(1);
    }
}

void main(void) {
    
    int tempo_delay = 50;
    //desabilitando comparadores para as portas RA0, RA1, RA2 e RA3 possam ser utilizadas como I/O digitais.    
    CMCONbits.CM0=1;
    CMCONbits.CM1=1;
    CMCONbits.CM2=1;    
    // outra forma CMCON=7; ou CMCON=0b0000111; ou CMCON=0x07;
    // onde CMCON=0x07 � CMCON=0b00000111; e CMCON=0x7 � ;CMCON=0b????0111
    
//Definindo dire��o das portas A, se ser� sa�da (0 - Output) ou entrada (1 - Input))
    TRISAbits.TRISA5=1; //**pino RA5 como entrada (Input)  ESSA PORTA N�O PODE SER SA�DA (DATASHEET) 
    TRISAbits.TRISA6=1; //pino RA6 como sa�da (Output)
    TRISAbits.TRISA7=1; //pino RA7 como sa�da (Output)    
//Outra forma TRISA=0b00100000                                                                                                                                                 
//                      |    |
//                     RA5  RA0
  
    TRISB=0b00000000; //todos os binos RBx como sa�da (Output)  ou em hexadecimal TRISB=0xff;
    //      | ...  |  
    //     RB7    RB0
    PORTAbits.RA4=1; //Porta RA4 = 1; //led deve ser ligado com �nodo no 5v e o c�todo no pic em s�rie com uma resist�ncia.  
    //Porta RA5 s� pode ser entrada (ver DATASHEET)
    PORTAbits.RA6=0; //Porta RA6 = 0;
    PORTAbits.RA7=0; //Porta RA7 = 0;
    
    PORTB=0b00000000; //todas as portas RBx = 0, n�vel l�gico baixo: 0 volts.
    //      | ... |  
    //     RB7   RB0
    
    while(1){
        if(PORTAbits.RA5){
            tempo_delay = TEMPO_DELAY4; 
        }else if(PORTAbits.RA6){
            tempo_delay = TEMPO_DELAY3; 
        }else if(PORTAbits.RA7){
            tempo_delay = TEMPO_DELAY2; 
        }else{
            tempo_delay = TEMPO_DELAY; 
        }
        PORTA=0xef;    //PORTA=0b1110 1111;
        PORTB=0b11111111;
        delay(tempo_delay);
        
        PORTAbits.RA0=0; //Porta RA0 = 0;
        PORTAbits.RA1=0; //Porta RA1 = 0;
        PORTAbits.RA2=0; //Porta RA2 = 0;
        PORTAbits.RA3=0; //Porta RA3 = 0;
        PORTAbits.RA4=1; //Porta RA4 = 0;
        //Porta RA5 s� pode ser entrada (ver DATASHEET)
        PORTAbits.RA6=0; //Porta RA6 = 0;
        PORTAbits.RA7=0; //Porta RA7 = 0;
        PORTB=0x00;      
        delay(tempo_delay);  
    }
    return;
}